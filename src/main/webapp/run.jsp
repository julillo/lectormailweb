<%-- 
    Document   : run
    Created on : 05-12-2017, 9:43:17
    Author     : Juan Lillo <ju.lillo.rojas@gmail.com>
--%>

<%@page import="com.jlillo.lectormail.config.Config"%>
<%@page import="com.jlillo.lectormail.controllers.LectorMailController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>LectorMail</title>
    </head>
    <body>
        <h1>Hello, from LectorMail:doCheck()</h1>
        <%!
            public String doCheck() {
                //change accordingly
                Config.MAIL_IN_HOST = "email.expert-choice.com";
                Config.MAIL_IN_PORT = 110;
                Config.MAIL_IN_TYPE = "pop3s";
                Config.STORE_BD_HOST = "jdbc:mysql://localhost:3307/qaeasy?serverTimezone=America/Santiago";
                Config.STORE_BD_USER = "root";
                Config.STORE_BD_PASSWORD = "";
                Config.STORE_BD_DRIVER = "com.mysql.jdbc.Driver";
                //change accordingly, stored localy for security
                String username = "data@expert-choice.com";
                String password = "45_%cFGh!as";

                LectorMailController lmc = new LectorMailController();
                lmc.check(username, password);
                return "Done! See server log for more info... Have a nice day! ;)";
            }
        %>
        <hr />
        <h3><%=doCheck()%></h3>
    </body>
</html>

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jlillo.lectormailweb.jobs;

import com.jlillo.lectormail.config.Config;
import com.jlillo.lectormail.controllers.LectorMailController;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author Juan Lillo <ju.lillo.rojas@gmail.com>
 */
public class ReadMailJob implements Job {

    private static final Log LOG = LogFactory.getLog(ReadMailJob.class);

    private LectorMailController lmc;
    private String username;
    private String password;

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        try {
            LOG.info("Starting cron LectorMail");
            //change accordingly
            Config.MAIL_IN_HOST = "email.expert-choice.com";
            Config.MAIL_IN_PORT = 110;
            Config.MAIL_IN_TYPE = "pop3s";
            Config.STORE_BD_HOST = "jdbc:mysql://localhost:3307/qaeasy?serverTimezone=America/Santiago";
            Config.STORE_BD_USER = "root";
            Config.STORE_BD_PASSWORD = "";
            Config.STORE_BD_DRIVER = "com.mysql.jdbc.Driver";
            //change accordingly, stored localy for security
            username = "data@expert-choice.com";
            password = "45_%cFGh!as";
            lmc = new LectorMailController();
            lmc.check(username, password);
            lmc = null;
            System.gc();
            LOG.info("End of cron LectorMail");
        } catch (Exception ex) {
            LOG.error(ex);
        }
    }

}

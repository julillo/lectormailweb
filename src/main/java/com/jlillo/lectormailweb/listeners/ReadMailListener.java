/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jlillo.lectormailweb.listeners;

import com.jlillo.lectormailweb.jobs.ReadMailJob;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

/**
 *
 * @author Juan Lillo <ju.lillo.rojas@gmail.com>
 */
public class ReadMailListener implements ServletContextListener {

    Scheduler scheduler = null;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("Context Initialized");

        try {
            // Setup the Job class and the Job group
            JobDetail job = JobBuilder.newJob(ReadMailJob.class).withIdentity(
                    "CronQuartzJob", "Group").build();

            // Create a Trigger that fires every 5 minutes.
            Trigger trigger = TriggerBuilder.newTrigger()
                    .withIdentity("TriggerName", "Group")
                    .withSchedule(
                            CronScheduleBuilder.cronSchedule("0 0 14 * * ?")
                    //            SimpleScheduleBuilder
                    //                    .simpleSchedule()
                    //                    .withIntervalInSeconds(10)
                    //                    .repeatForever()
                    )
                    .build();

            // Setup the Job and Trigger with Scheduler & schedule jobs
            scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.start();
            scheduler.scheduleJob(job, trigger);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("Context Destroyed");
        try {
            scheduler.shutdown();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

}
